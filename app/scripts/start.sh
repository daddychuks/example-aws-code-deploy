#!/bin/bash

killall -9 node || true
nohup /home/ec2-user/.nvm/versions/node/v14.5.0/bin/node /opt/apps/index.js > /opt/apps/start.log 2>&1 &
